package models;

import com.cooolem.ctp.module.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
public class UserRate extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    public Date date;
    public String comment;



}
