package models;

import com.cooolem.ctp.module.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Region extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    public String province;
    public String city;
    public String district;

    @OneToMany(fetch=FetchType.EAGER,mappedBy = "region")
    @JsonIgnore
    public Set<TrainingInstitution> trainingInsts;

}
