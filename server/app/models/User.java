package models;

import com.cooolem.ctp.module.base.BaseModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class User extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    public String phone;
    public String passWord;
    public String nickName;
    public String avatar;
    public Date birthday;
    public Integer gender;
    public Boolean active;



}
