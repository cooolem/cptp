package models;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by winston on 2015/5/15.
 */
public interface BusinessRepository  extends CrudRepository<Business, Long> {
}
