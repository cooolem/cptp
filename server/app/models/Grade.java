package models;

import com.cooolem.ctp.module.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Grade extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    public Integer type;
    public Integer grade;

    @OneToMany(fetch=FetchType.EAGER,mappedBy = "grade")
    @JsonIgnore
    public Set<TrainingInstitution> trainingInsts;

}
