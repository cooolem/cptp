package models;

import java.util.List;

/**
 * Created by winston on 2015/4/20.
 */
public class Campaign {

    private String name;
    private Integer targetCountry;
    private Integer bannerSize;
    private Boolean random;

    private List<Banner> banners;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTargetCountry() {
        return targetCountry;
    }

    public void setTargetCountry(Integer targetCountry) {
        this.targetCountry = targetCountry;
    }

    public Integer getBannerSize() {
        return bannerSize;
    }

    public void setBannerSize(Integer bannerSize) {
        this.bannerSize = bannerSize;
    }

    public List<Banner> getBanners() {
        return banners;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }

    public Boolean getRandom() {
        return random;
    }

    public void setRandom(Boolean random) {
        this.random = random;
    }
}
