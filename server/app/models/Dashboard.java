package models;

import java.util.List;

/**
 * Created by winston on 2015/4/20.
 */
public class Dashboard {

    private Integer bannerId;
    private String bannerName;
    private Integer clickCount;
    private List<String> ipHistory;

    public Integer getBannerId() {
        return bannerId;
    }

    public void setBannerId(Integer bannerId) {
        this.bannerId = bannerId;
    }

    public List<String> getIpHistory() {
        return ipHistory;
    }

    public void setIpHistory(List<String> ipHistory) {
        this.ipHistory = ipHistory;
    }

    public Integer getClickCount() {
        return clickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }

    public String getBannerName() {
        return bannerName;
    }

    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }
}
