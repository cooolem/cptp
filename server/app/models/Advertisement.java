package models;

import com.cooolem.ctp.module.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.persistence.criteria.Predicate;
import java.util.Date;

@Entity
public class Advertisement extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn( name="trainInstId" )
    @JsonIgnore
    public TrainingInstitution trainingInst;
    public String name;
    public String thumbnail;
    public Date deadline;
    public String description;
    public String htmlInfo;
    public Boolean isPres;

}
