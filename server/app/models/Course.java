package models;

import com.cooolem.ctp.module.base.BaseModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
public class Course extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    public String name;

    @ManyToMany(mappedBy="courses",fetch=FetchType.EAGER)
    @JsonIgnore
    public List<TrainingInstitution> trainingInsts;

}
