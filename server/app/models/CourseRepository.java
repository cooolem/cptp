package models;

import org.springframework.data.repository.CrudRepository;

/**
 * Author: winston add  2015/5/18.
 */
public interface CourseRepository extends CrudRepository<Course, Long> {
}
