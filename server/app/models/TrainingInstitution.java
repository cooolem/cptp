package models;

import com.cooolem.ctp.module.base.BaseModel;

import javax.persistence.*;
import java.util.Set;

@Entity
public class TrainingInstitution extends BaseModel {

    @Id
    @GeneratedValue
    public Long id;
    public String name;
    public String address;
    public String phone;
    public String landline;
    public String adPic;
    public String thumbnailPic;
    public String picture;
    public String description;
    public String htmlInfo;
    public Boolean isHomePage;

    @OneToMany(cascade =CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "trainingInst")
    public Set<Advertisement> advertisements;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="TrainingInstCourseMap",inverseJoinColumns = @JoinColumn(name="courseId"),joinColumns = @JoinColumn(name="traingInstId"))
    public Set<Course> courses ;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    @JoinColumn(name = "gradeId")
    public Grade grade;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER )
    @JoinColumn(name = "regionId")
    public Region region;

}
