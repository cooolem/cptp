package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Person;
import play.libs.Json;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class RTDataSender
{

    private static final ScheduledExecutorService SENDER_EXEC =
            Executors.newSingleThreadScheduledExecutor();

    /** The list of clients that wish to be sent real-time data over a WebSocket. */
    private static final List<ClientWebSocket> clientSockets = new ArrayList<ClientWebSocket>();

    /** Default number of columns in the PatientView 
    private final static Integer NUM_COLUMNS = 4;*/
    /** Default number of patients in the PatientView. */
    private final static Integer DEF_NUM_PATIENTS = 16;
    
    //private static LayoutDescriptor layoutDesc;

    static
    {
        // get the number of patients out of the layout.properties file
        int numPatients = PropertyFileReader.readIntegerProperty("conf/layout.properties", "numPatients", DEF_NUM_PATIENTS);

       // PatientData.init(numPatients);
        
        // gets the number of columns out of the layout.properties file
       /* int numColumns = PropertyFileReader.readIntegerProperty("conf/layout.properties", "numColumns", NUM_COLUMNS);       
        layoutDesc = new LayoutDescriptor(numColumns);*/

        SENDER_EXEC.scheduleAtFixedRate(new Sender(), 250, 250, TimeUnit.MILLISECONDS);
    }

    /**
     * Holder of the singleton instance of this class.
     */
    private static final class RTDataSenderHolder
    {
        static final RTDataSender instance = new RTDataSender();
    }

    /**
     * @return the singleton instance of this class
     */
    public static RTDataSender getInstance()
    {
        return RTDataSenderHolder.instance;
    }

    /**
     * A private constructor to disable outside instantiation.
     */
    private RTDataSender()
    {
    }

    public void addClient(ClientWebSocket client)
    {
        synchronized(clientSockets)
        {
            clientSockets.add(client);
        }

        System.out.println("controllers.RTDataSender.addClient("+client+"): " +
                clientSockets.size() + " total clients");
    }

    public void removeClient(ClientWebSocket client)
    {
        synchronized(clientSockets)
        {
            clientSockets.remove(client);
        }

        System.out.println("controllers.RTDataSender.removeClient("+client+"): " +
                clientSockets.size() + " remaining clients");
    }
    


    private static class Sender implements Runnable
    {
        private int tick = 0;
        private final int waveformRate = 60;
        long accumulatedTimeNanos = 0;

        @Override
        public void run()
        {
            long startNanos = System.nanoTime();

            List<ClientWebSocket> clients;
            synchronized(clientSockets)
            {
                clients = new ArrayList<ClientWebSocket>(clientSockets);
            }

            // Prepare the buffer of waveform data to be sent.
            int [] sendBuffer = new int[waveformRate];
            System.arraycopy(ecgWaveform, 0, sendBuffer, 0, waveformRate);

            // Send data to all the clients.
            final Person person = new Person();
            person.firstname = "Bruce";
            person.surname = "Smith";

            JsonNode node = Json.toJson(person);
            for (ClientWebSocket client : clients)
            {
                client.getWebSocketOut().write(node);
            }

            

            // Rotate the ECG waveform data in preparation for the next send.
            ecgWaveform = rotateData(ecgWaveform, waveformRate);
/*
            accumulatedTimeNanos += System.nanoTime() - startNanos;

            if (tick == 0)
            {
                long avgMillis = TimeUnit.NANOSECONDS.toMillis(accumulatedTimeNanos / 8);
                System.out.println("Average time ("+clientSockets.size()+" clients): " +
                                   avgMillis + " msec");
                accumulatedTimeNanos = 0;
            }
*/
            tick = (++tick % 8);
        }

        private int[] rotateData(int[] buffer, int offset)
        {
            int front[] = buffer;
            int temporary[] = new int[buffer.length];
            System.arraycopy(buffer, offset, temporary, 0, buffer.length - offset);
            System.arraycopy(front, 0, temporary, buffer.length - offset, offset);

            return temporary;
        }

        private double newNumericValue(double currValue)
        {
            double adjust = 0.5;
            final double HI_LIMIT  = 150.0;
            final double LOW_LIMIT = 35.0;

            // Tweak the adjstment to the current value if the current value is
            // drifting low or high.
            if (currValue < LOW_LIMIT)
            {
                adjust -= (LOW_LIMIT - currValue) / 20;
            }
            else if (currValue > HI_LIMIT)
            {
                adjust += (currValue - HI_LIMIT) / 20;
            }

            return currValue + (int) Math.round(16 * (Math.random() - adjust));
        }
    }

    private static int[] ecgWaveform = {
        -16, -8, -11, -5, -12, -2, -9, -1, -13, -2, -9, 0, -12, -2, -10, -1,
        -19, -20, -39, -39, -5, 78, 166, 267, 325, 378, 336, 280, 170, 63, -6, -31,
        -53, -54, -27, 1, -10, -10, -17, -6, -8, -4, -15, -4, -8, -6, -15, 1,
        -5, -2, -10, 7, -2, 0, -5, 8, 2, 5, 1, 17, 9, 15, 6, 20,
        16, 21, 15, 28, 22, 27, 21, 32, 28, 40, 30, 44, 40, 50, 45, 62,
        58, 65, 58, 74, 68, 75, 61, 70, 60, 61, 48, 52, 39, 37, 19, 16,
        -6, 0, -10, -4, -18, -9, -18, -12, -22, -11, -18, -8, -21, -8, -16, -7,
        -17, -7, -14, -8, -18, -9, -15, -6, -19, -7, -15, -5, -16, -7, -14, -5,
        -16, -7, -13, -4, -15, -6, -14, -4, -14, -7, -13, -5, -16, -12, -16, -6,
        -17, -11, -13, -3, -13, -8, -15, -7, -14, -6, -13, -2, -12, -5, -13, -1,
        -13, -7, -11, -1, -13, -4, -12, -2, -13, -5, -12, 0, -11, -7, -13, -6,
        -12, -10, -14, 0, -11, -6, -12, 3, -9, -7, -12, -2, -10, -1, -13, -2,
        -9, -2, -11, -2, -10, -2, -12, -3, -11, -5, -14, -2, -11, -3, -11, -1,
        -9, -3, -12, -1, -10, -7, -14, -7, -10, 3, 3, 17, 21, 30, 28, 40,
        38, 47, 37, 50, 38, 40, 26, 33, 15, 13, -2, 3, -10, -3, -13, 0, -11, -5
    };
}
