package controllers;

import com.cooolem.ctp.module.service.AdvertisementService;
import com.cooolem.ctp.module.service.CourseService;
import com.cooolem.ctp.module.service.EducationBusinessService;
import com.cooolem.ctp.module.vo.IndexPageVo;
import com.cooolem.ctp.module.vo.TrainingInstVO;
import com.fasterxml.jackson.databind.JsonNode;
import com.cooolem.ctp.module.base.AbstractBaseController;
import models.*;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.mvc.Controller;
import play.mvc.Result;


import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.*;

@Named
public class MainPage extends Controller {

    private final TrainingInstRepository trainingInstRepository;
    private final CourseRepository courseRepository;


    @Transactional
    public  Result getIndexData() {

        IndexPageVo indexPageVo = new IndexPageVo();
       // final TrainingInstitution retrievedBusiness = trainingInstRepository.findOne(1l);

       // String trainingInst = play.libs.Json.stringify(json);
        List<TrainingInstitution> traningInsts = trainingInstService.displayAdvertisment(1l);
        indexPageVo.setTrainingInstsPres(traningInsts);

        indexPageVo.setTrainingInstsNear(traningInsts);

        List<Advertisement> advertisements = advertisementService.getPresAdvertisement();
        indexPageVo.setAdvertisements(advertisements);

        List<Course> courses = courseService.getCourses();
        indexPageVo.setCourses(courses);

        JsonNode json = play.libs.Json.toJson(indexPageVo);
        return ok(json);
    }

    @Transactional
    public Result searchTrainingInstByCourse( Long id ){

      //  List<TrainingInstitution> trainingInsts = new ArrayList<TrainingInstitution>();

        List<TrainingInstitution> trainingInsts = trainingInstService.searchTrainingInstByCourse(id);
        //this.courseService.

        JsonNode json = play.libs.Json.toJson(trainingInsts);
        return ok(json);
    }

    public Result saveDummyData() {

        final TrainingInstitution traningInst = new TrainingInstitution();
        traningInst.name = "东方娃娃";
        traningInst.adPic="adpic.png";
        traningInst.address="四季晶华";
        traningInst.description="权威的培训机构";
        //traningInst.gradeId = 2l;
        traningInst.phone="1388888888";
        traningInst.landline="62888888";
        traningInst.thumbnailPic="thumbnail.png";
        traningInst.picture="picture.png";

        traningInst.isHomePage=true;

        Set<Advertisement> advertisements = populateAds(traningInst);

        Set<Course> courses = populateCourse(traningInst);

        traningInst.advertisements = advertisements;
        traningInst.courses = courses;

        Grade grade = new Grade();
        grade.type = 2;
        grade.grade = 3;
        traningInst.grade = grade;

        Region region = new Region();
        region.province = "江苏省";
        region.city = "苏州市";
        region.district = "吴中区";
        traningInst.region = region;

        final TrainingInstitution savedBusiness = trainingInstRepository.save(traningInst);

        final TrainingInstitution retrievedBusiness = trainingInstRepository.findOne(savedBusiness.id);
        //trainingInstRepository.

        // Deliver the index page with a message showing the id that was generated.

        return ok(views.html.index.render("Found id: " + retrievedBusiness.id + " of person/people"));
    }

    private Set<Course> populateCourse(TrainingInstitution traningInst) {
        Set<Course> courses = new HashSet<Course>();

        Course course = new Course();
        course.name="音乐";
        //course.id= 1l;
        courses.add(course);

        course = new Course();
        course.name="舞蹈";
        //course.id= 2l;
        courses.add(course);

        course = new Course();
        course.name="美术";
        //course.id= 3l;
        courses.add(course);

        return courses;
    }

    private Set<Advertisement> populateAds(TrainingInstitution traningInst) {
        Set<Advertisement> advertisements = new HashSet<Advertisement>();

        Advertisement advertisement = new Advertisement();
        advertisement.trainingInst = traningInst;
        advertisement.deadline = new Date();
        advertisement.thumbnail = "adc.jpg";
        advertisement.name = "www.baidu.com";
        advertisements.add(advertisement);

        advertisement = new Advertisement();
        advertisement.trainingInst = traningInst;
        advertisement.deadline = new Date();
        advertisement.thumbnail = "235.jpg";
        advertisement.name = "www.google.com";
        advertisements.add(advertisement);
        return advertisements;
    }

    @Inject
    public MainPage(final TrainingInstRepository trainingInstRepository,final CourseRepository courseRepository) {
        this.trainingInstRepository = trainingInstRepository;
        this.courseRepository= courseRepository;
    }

    private EducationBusinessService trainingInstService;
    private AdvertisementService advertisementService;
    private CourseService courseService;

    @Autowired
    public void setTrainingInstService(EducationBusinessService trainingInstService) {
        this.trainingInstService = trainingInstService;
    }

    @Autowired

    public void setAdvertisementService(AdvertisementService advertisementService) {
        this.advertisementService = advertisementService;
    }

    @Autowired
    public void setCourseService(CourseService courseService) {
        this.courseService = courseService;
    }
}
