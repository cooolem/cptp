package controllers;

import com.fasterxml.jackson.databind.JsonNode;

import play.libs.F.Callback0;
import play.mvc.WebSocket;

public class ClientWebSocket extends WebSocket<JsonNode>
{
    private In<JsonNode> wsIn;
    private Out<JsonNode> wsOut;

    private static final RTDataSender RT_SENDER = RTDataSender.getInstance();

    // Called when the Websocket Handshake is done.
    @Override
    public void onReady(In<JsonNode> in, Out<JsonNode> out)
    {
        wsIn = in;
        wsOut = out;

        RT_SENDER.addClient(this);

        // When the socket is closed.
        wsIn.onClose(new Callback0()
        {
           @Override
           public void invoke()
           {
               // Send a Quit message to the room.
               RT_SENDER.removeClient(ClientWebSocket.this);
           }
        });
    }

    public In<JsonNode> getWebSocketIn()
    {
        return wsIn;
    }

    public Out<JsonNode> getWebSocketOut()
    {
        return wsOut;
    }
}
