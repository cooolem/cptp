package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.cooolem.ctp.module.base.AbstractBaseController;
import models.*;
import play.libs.Json;
import play.mvc.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.File;
import java.util.*;

/**
 * The main set of web services.
 */
@Named
@Singleton
public class Application extends AbstractBaseController {

    private final PersonRepository personRepository;
    public static Campaign campaign = new Campaign();
    public static Map<Integer,Dashboard> dashboards = new HashMap<Integer,Dashboard>();

    static{
        campaign.setName("Campaign Name");
        campaign.setTargetCountry(0);
        campaign.setBannerSize(0);
        campaign.setRandom(false);

        List<Banner> banners = new ArrayList<Banner>();

        Banner banner = new Banner();
        banner.setId(1);
        banner.setName("Sian");
        banner.setUrl("www.sian.com");
        banners.add(banner);

        banner = new Banner();
        banner.setId(2);
        banner.setName("Baidu");
        banner.setUrl("www.sian.com");

        banners.add(banner);

        banner = new Banner();
        banner.setId(3);
        banner.setName("Google");
        banner.setUrl("www.google.com");

        banners.add(banner);


        campaign.setBanners(banners);

    }

    // We are using constructor injection to receive a repository to support our desire for immutability.
    @Inject
    public Application(final PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public  Result displayAd() {

        //campaign
        return ok(views.html.display.render(campaign));

    }

    public  Result addAd() {


        JsonNode  json = Json.toJson(campaign);

        String campaign = Json.stringify(json);
        return ok(views.html.addAd.render(campaign));

    }

    public  Result dashboard() {

        List<Dashboard> list = new ArrayList<Dashboard>();

        list.addAll(dashboards.values());

        JsonNode  json = Json.toJson(list);

        return ok(views.html.dashboard.render(Json.stringify(json)));
    }

    /**
    public  Result conversionDashboard() {

        final Person person = new Person();
        person.firstname = "Bruce";
        person.surname = "Smith";

        JsonNode  json = Json.toJson(person);
        return ok(json);
    }
     */

    public WebSocket<JsonNode> conversionDashboard(){
        ClientWebSocket ws = new ClientWebSocket();
        return ws;
    }

    @BodyParser.Of(BodyParser.Json.class)
    public  Result recordAd() {

        JsonNode json = request().body().asJson();
        String name = json.findPath("name").textValue();
        Integer targetCountry = json.findPath("targetCountry").intValue();
        Integer bannerSize = json.findPath("bannerSize").intValue();
        Iterator<JsonNode> bannerNodes = json.findPath("banners").elements();

        //List<JsonNode> bannerNodes = json.findParents("banners");
        List<Banner> banners = new ArrayList<Banner>();
        while (bannerNodes.hasNext()){
            JsonNode node = bannerNodes.next();
            Banner banner = new Banner();
            String bannerName = node.findPath("name").textValue();
            String bannerUrl = node.findPath("url").textValue();
            Integer id = node.findPath("id").intValue();

            banner.setId(id);
            banner.setName(bannerName);
            banner.setUrl(bannerUrl);
            banners.add(banner);
        }

        campaign.setName(name);
        campaign.setBannerSize(bannerSize);
        campaign.setBanners(banners);
        return ok(views.html.addAd.render(""));
    }

    public  Result statistics(Integer bannerId){

        String ip = request().remoteAddress();

        if(dashboards.containsKey(bannerId)){
            Dashboard dashboard = dashboards.get(bannerId);
            Integer count = dashboard.getClickCount();
            dashboard.setClickCount(count + 1);
            if (!dashboard.getIpHistory().contains(ip)) {
                dashboard.getIpHistory().add(ip);
            }
        }else{
            String bannerName = getBannerName(bannerId);
            Dashboard dashboard = new Dashboard();
            dashboard.setBannerId(bannerId);
            dashboard.setClickCount(1);
            dashboard.setBannerName(bannerName);

            List<String> ipHistory = new ArrayList<String>();
            ipHistory.add(ip);

            dashboard.setIpHistory(ipHistory);
            dashboards.put(bannerId,dashboard);

        }

        DashboardRoom.defaultRoom.tell(new DashboardRoom.Talk(ip,new ArrayList(dashboards.values())),null);

        return ok(views.html.display.render(campaign));
    }

    private String getBannerName(Integer bannerId){
        List<Banner> banners = campaign.getBanners();
        for(Banner banner:banners){
            if(bannerId.equals(banner.getId())){
                return banner.getName();
            }
        }

        return null;
    }

    public static Result dashboardJs(){
        return  ok(views.js.dashboard.render());
    }

    public Result index() {

        // For fun we save a new person and then find that one we've just saved. The id is auto generated by
        // the db so we know that we're round-tripping to the db and back in order to demonstrate something
        // interesting. Spring Data takes care of transactional concerns and the following code is all
        // executed on the same thread (a requirement of the JPA entity manager).

        final Person person = new Person();
        person.firstname = "Bruce";
        person.surname = "Smith";

        final Person savedPerson = personRepository.save(person);

        final Person retrievedPerson = personRepository.findOne(savedPerson.id);

        // Deliver the index page with a message showing the id that was generated.

        return ok(views.html.index.render("Found id: " + retrievedPerson.id + " of person/people"));
    }

    /**
     * Handle the chat websocket.
     */
    public static WebSocket<JsonNode> chat() {
        return new WebSocket<JsonNode>() {

            // Called when the Websocket Handshake is done.
            public void onReady(WebSocket.In<JsonNode> in, WebSocket.Out<JsonNode> out){

                // Join the dashboard room.
                try {
                    DashboardRoom.join("user1", in, out);
                    DashboardRoom.defaultRoom.tell(new DashboardRoom.Talk("One",new ArrayList(dashboards.values())),null);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
    }

    public static Results uploadPic(File pic){

      //  Files.

       // Play.getFile("",app)
        return null;
    }
}
