package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * Utility class used for reading and returning data from
 * property files.
 */
public class PropertyFileReader {


    /**
     * Read the given property file for the specific property key,
     * and return the integer value or the default integer if the property
     * does not exist, file does not exist, or value is not an integer.
     * @param filePath
     * @param propertyKey
     * @param defaultInteger
     * @return
     */
    public static Integer readIntegerProperty(String filePath, String propertyKey, Integer defaultInteger) {

        String propertyValue = PropertyFileReader.readProperty(filePath, propertyKey);
        if(propertyValue != "") {
            try {
                return Integer.parseInt(propertyValue);
            } catch(NumberFormatException e) {
                // should log this
                System.err.println(propertyKey+" property value is not an int.  Setting to default.");
            }
        }
        
        return defaultInteger;
    }
    
    
    /**
     * Read the property file for the given propertyKey and return that, or
     * an empty string if there was an error or the property couldn't be found.
     * @param filePath
     * @param propertyKey
     * @return String
     */
    private static String readProperty(String filePath, String propertyKey) {

        FileInputStream in =  null;
        try {
            in = new FileInputStream(filePath);
            Properties props = new Properties();
            props.load(in);
            if(props.containsKey(propertyKey)) {
                return props.getProperty(propertyKey);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Property file " + filePath + " not found."); // should log this
        } catch (IOException e) {
            System.err.println("IOException when reading property file " + filePath + "."); // should log this
        } finally {
            if(in != null) {
                try { 
                    in.close(); 
                } catch(IOException e) {
                    System.err.println("IOException when closing property file " + filePath + "."); // should log this
                }
            }
        }
        
        return ""; // return an empty String if couldn't get property
    }

}
