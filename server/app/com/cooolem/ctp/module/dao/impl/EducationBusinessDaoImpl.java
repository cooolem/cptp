package com.cooolem.ctp.module.dao.impl;

import com.cooolem.ctp.module.base.HibernateDao;
import com.cooolem.ctp.module.dao.EducationBusinessDao;
import models.Advertisement;
import models.PersonRepository;
import models.TrainingInstitution;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class EducationBusinessDaoImpl extends HibernateDao<TrainingInstitution,Long> implements EducationBusinessDao {
    /**
     * HibernateDao Constructor, set entityClass
     *
     */
    public EducationBusinessDaoImpl() {
        super(TrainingInstitution.class);
    }

    // private final PersonRepository personRepository;

    @Override
    public List<TrainingInstitution> displayAdveritisment(Long regionId) {
        StringBuilder hql = new StringBuilder();
        hql.append(" from TrainingInstitution A ");

        return hqlList(hql.toString(),new HashMap<String, Object>());
    }

    @Override
    public List<TrainingInstitution> searchTrainingInstByCourse(Long courseId) {
        StringBuilder hql = new StringBuilder();
        hql.append(" select A ");
        hql.append(" from TrainingInstitution A ");
        hql.append(" left join A.courses B ");
        hql.append(" where B.id=:courseId ");

        Map para = new HashMap<String, Object>();
        para.put("courseId",courseId);

        return hqlList(hql.toString(),para);
    }


}
