package com.cooolem.ctp.module.dao.impl;

import com.cooolem.ctp.module.base.HibernateDao;
import com.cooolem.ctp.module.dao.AdvertisementDao;
import com.cooolem.ctp.module.dao.CourseDao;
import models.Advertisement;
import models.Course;
import models.TrainingInstitution;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;


@Repository
public class CourseDaoImpl extends HibernateDao<Course,Long> implements CourseDao {
    /**
     * HibernateDao Constructor, set entityClass
     *
     */
    public CourseDaoImpl() {
        super(Course.class);
    }

    // private final PersonRepository personRepository;


    @Override
    public List<Course> getCourses() {
        StringBuilder hql = new StringBuilder();
        hql.append(" from Course A ");
        return hqlList(hql.toString(),new HashMap<String, Object>());
    }

    @Override
    public Course searchTrainingInstByCourse( Long id ) {
        return get(id);
    }
}
