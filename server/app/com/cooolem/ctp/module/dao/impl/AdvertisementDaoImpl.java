package com.cooolem.ctp.module.dao.impl;

import com.cooolem.ctp.module.base.HibernateDao;
import com.cooolem.ctp.module.dao.AdvertisementDao;
import com.cooolem.ctp.module.dao.EducationBusinessDao;
import models.Advertisement;
import models.TrainingInstitution;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;


@Repository
public class AdvertisementDaoImpl extends HibernateDao<Advertisement,Long> implements AdvertisementDao {
    /**
     * HibernateDao Constructor, set entityClass
     *
     */
    public AdvertisementDaoImpl() {
        super(Advertisement.class);
    }

    // private final PersonRepository personRepository;

    @Override
    public List<Advertisement> getPresAdvertisement() {
        StringBuilder hql = new StringBuilder();
        hql.append(" from Advertisement A ");
        hql.append( " where A.isPres=1 ");
        return hqlList(hql.toString(),new HashMap<String, Object>());
    }
}
