package com.cooolem.ctp.module.dao;

import models.Advertisement;
import models.Course;
import models.TrainingInstitution;

import java.util.List;

/**
 * Created by winston on 2015/5/14.
 */
public interface CourseDao {

    List<Course> getCourses();

    Course searchTrainingInstByCourse(Long id);

}
