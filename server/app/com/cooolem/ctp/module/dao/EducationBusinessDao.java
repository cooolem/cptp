package com.cooolem.ctp.module.dao;

import models.Advertisement;
import models.TrainingInstitution;

import java.util.List;

/**
 * Created by winston on 2015/5/14.
 */
public interface EducationBusinessDao {
    public List<TrainingInstitution> displayAdveritisment(Long regiondId);

    List<TrainingInstitution> searchTrainingInstByCourse(Long courseId);
}
