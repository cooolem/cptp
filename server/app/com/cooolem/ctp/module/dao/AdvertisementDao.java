package com.cooolem.ctp.module.dao;

import models.Advertisement;
import models.TrainingInstitution;

import java.util.List;

/**
 * Created by winston on 2015/5/14.
 */
public interface AdvertisementDao {

    List<Advertisement> getPresAdvertisement();
}
