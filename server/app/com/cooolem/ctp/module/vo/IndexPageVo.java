package com.cooolem.ctp.module.vo;


import models.Advertisement;
import models.Course;
import models.TrainingInstitution;

import java.util.List;

public class IndexPageVo {

    private List<TrainingInstitution> trainingInstsPres;
    private List<TrainingInstitution> trainingInstsNear;
    private List<Course> courses;
    private List<Advertisement> advertisements;

    public List<TrainingInstitution> getTrainingInstsPres() {
        return trainingInstsPres;
    }

    public void setTrainingInstsPres(List<TrainingInstitution> trainingInstsPres) {
        this.trainingInstsPres = trainingInstsPres;
    }

    public List<TrainingInstitution> getTrainingInstsNear() {
        return trainingInstsNear;
    }

    public void setTrainingInstsNear(List<TrainingInstitution> trainingInstsNear) {
        this.trainingInstsNear = trainingInstsNear;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(List<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }
}
