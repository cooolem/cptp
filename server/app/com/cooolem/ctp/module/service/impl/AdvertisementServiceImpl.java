package com.cooolem.ctp.module.service.impl;

import com.cooolem.ctp.module.dao.AdvertisementDao;
import com.cooolem.ctp.module.dao.EducationBusinessDao;
import com.cooolem.ctp.module.service.AdvertisementService;
import com.cooolem.ctp.module.service.EducationBusinessService;
import models.Advertisement;
import models.TrainingInstitution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import play.db.jpa.Transactional;

import java.util.List;


@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {

    @Override
    public List<Advertisement> getPresAdvertisement() {
        return advertisementDao.getPresAdvertisement();
    }

    private AdvertisementDao advertisementDao;

    @Autowired
    public void setAdvertisementDao(AdvertisementDao advertisementDao) {
        this.advertisementDao = advertisementDao;
    }
}
