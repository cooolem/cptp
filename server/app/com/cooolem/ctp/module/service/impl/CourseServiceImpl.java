package com.cooolem.ctp.module.service.impl;

import com.cooolem.ctp.module.dao.AdvertisementDao;
import com.cooolem.ctp.module.dao.CourseDao;
import com.cooolem.ctp.module.service.AdvertisementService;
import com.cooolem.ctp.module.service.CourseService;
import models.Advertisement;
import models.Course;
import models.TrainingInstitution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import play.db.jpa.Transactional;

import java.util.List;


@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    @Override
    public List<Course> getCourses() {
        return courseDao.getCourses();
    }

    @Override
    public Course searchTrainingInstByCourse(Long id) {
        return courseDao.searchTrainingInstByCourse(id);
    }

    private CourseDao courseDao;

    @Autowired
    public void setCourseDao(CourseDao courseDao) {
        this.courseDao = courseDao;
    }

}
