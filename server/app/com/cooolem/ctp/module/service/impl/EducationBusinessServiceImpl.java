package com.cooolem.ctp.module.service.impl;

import com.cooolem.ctp.module.dao.EducationBusinessDao;
import com.cooolem.ctp.module.service.EducationBusinessService;
import models.TrainingInstitution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import play.db.jpa.Transactional;

import java.util.List;


@Service
@Transactional
public class EducationBusinessServiceImpl implements EducationBusinessService {

    @Override

    public List<TrainingInstitution> displayAdvertisment(Long regiondId){
        return  this.educationBusinessDao.displayAdveritisment(regiondId);
    }

    @Override
    public List<TrainingInstitution> searchTrainingInstByCourse(Long id) {
        return this.educationBusinessDao.searchTrainingInstByCourse(id);
    }

    private EducationBusinessDao educationBusinessDao;

    @Autowired
    public void setEducationBusinessDao(EducationBusinessDao educationBusinessDao) {
        this.educationBusinessDao = educationBusinessDao;
    }
}
