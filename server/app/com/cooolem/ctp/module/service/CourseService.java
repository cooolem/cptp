package com.cooolem.ctp.module.service;

import models.Advertisement;
import models.Course;
import models.TrainingInstitution;

import java.util.List;

public interface CourseService {


    List<Course> getCourses();

    Course searchTrainingInstByCourse(Long id);

}
