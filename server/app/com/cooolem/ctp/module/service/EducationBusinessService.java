package com.cooolem.ctp.module.service;

import models.Course;
import models.TrainingInstitution;

import java.util.List;

public interface EducationBusinessService {
    List<TrainingInstitution> displayAdvertisment(Long regiondId);

    List<TrainingInstitution> searchTrainingInstByCourse(Long id);
}
