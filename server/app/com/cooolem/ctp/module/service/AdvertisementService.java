package com.cooolem.ctp.module.service;

import models.Advertisement;
import models.TrainingInstitution;

import java.util.List;

public interface AdvertisementService {

    List<Advertisement> getPresAdvertisement();
}
