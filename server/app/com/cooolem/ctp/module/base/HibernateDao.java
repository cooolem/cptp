package com.cooolem.ctp.module.base;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import play.db.jpa.JPA;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class HibernateDao<T, PK extends Serializable> {

    /**
     * HibernateDao Constructor, set entityClass
     *
     * @param entityClass the actual entity class that this DAO should use
     */

    public HibernateDao(Class<? extends T> entityClass) {
        //this.sessionFactory = JPA.em().unwrap(Session.class).getSessionFactory();
       // this.hibernateTemplate = new HibernateTemplate(sessionFactory);
        this.entityClass = entityClass;
    }



    /**
     * Get current session.
     *
     * @return current session
     */
    public Session getSession() {
        return JPA.em().unwrap(Session.class);
    }

    private HibernateTemplate getHibernateTemplate(){

        return new HibernateTemplate(getSession().getSessionFactory());

    }


    //--------------------------------------------
    // Basic CRUD Function
    //--------------------------------------------

    @SuppressWarnings("unchecked")
    public PK add(T object) {
        Assert.notNull(object, "Parameter object must not be null!");
        return (PK) getSession().save(object);
    }

    public void update(T object) {
        Assert.notNull(object, "Parameter object must not be null!");
        getSession().update(object);
        getSession().flush();
    }

    public void save(T object) {
        Assert.notNull(object, "Parameter object must not be null!");
        getSession().saveOrUpdate(object);
        getSession().flush();
    }

    public void delete(PK id) {
        getHibernateTemplate().delete(this.get(id));
    }

    public T get(PK id) {
        T entity = (T) getHibernateTemplate().get(this.entityClass, id);
        if (entity == null) {
            log.info("Uh oh, '" + this.entityClass + "' object with id '" + id + "' not found...");
            throw new ObjectRetrievalFailureException(this.entityClass, id);
        }
        return entity;
    }

    public boolean exists(PK id) {
        T entity = (T) getHibernateTemplate().get(this.entityClass, id);
        return entity != null;
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        // Java doesn't support casting List<? extends T> to List<T>,
        // so we have to do this manually
        List<? extends T> list = getHibernateTemplate().loadAll(this.entityClass);
        if (list == null) {
            return null;
        }
        List<T> result = new ArrayList<T>();
        for (Object item : list) {
            result.add((T) item);
        }
        return result;
    }

    //----------------------------------------
    // HQL Unique
    //----------------------------------------

    @SuppressWarnings("unchecked")
    protected T hqlUnique(String hql, Map<String, Object> params) {
        Query query = getSession().createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return (T) query.uniqueResult();
    }

    protected Object hqlBeanUnique(String hql, Map<String, Object> params) {
        Query query = getSession().createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return query.uniqueResult();
    }


    //----------------------------------------
    // SQL Unique
    //----------------------------------------

    @SuppressWarnings("unchecked")
    protected T sqlUnique(String sql, Map<String, Object> params) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.addEntity(entityClass);
        return (T) query.uniqueResult();
    }

    //----------------------------------------
    // HQL List
    //----------------------------------------

    @SuppressWarnings("unchecked")
    protected List<T> hqlList(String hql, Map<String, Object> params) {
        Session session = getSession();
        Query query = session.createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return query.list();
    }

    @SuppressWarnings("unchecked")
    protected List<T> hqlList(String hql, Map<String, Object> params, String filterName) {
        Session session = getSession();
        Query query = session.createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        if (StringUtils.hasText(filterName)) {
            session.enableFilter(filterName);
        }
        return query.list();
    }

    @SuppressWarnings("unchecked")
    protected List<?> hqlQuery(String hql, Map<String, Object> params) {
        Session session = getSession();
        Query query = session.createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return query.list();
    }

    //----------------------------------
    // Paginated HQL List
    //----------------------------------
    @SuppressWarnings("unchecked")
    protected List<?> paginatedHqlResult(String hql, Map<String, Object> params, int page, int pageSize) {
        Query query = getSession().createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.setFirstResult((page - 1) * pageSize);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    protected List<?> paginatedSqlResult(String sql, Map<String, Object> params, int page, int pageSize, Class cls) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.addEntity(cls);
        query.setFirstResult((page - 1) * pageSize);
        query.setMaxResults(pageSize);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    protected List<?> paginatedSqlResult(String sql, Map<String, Object> params, int page, int pageSize, Map<String, Class> classMap) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        for (String key : classMap.keySet()) {
            query.addEntity(key, classMap.get(key));
        }
        query.setFirstResult((page - 1) * pageSize);
        query.setMaxResults(pageSize);
        return query.list();
    }

    protected void executeHQL(String hql, Map<String, Object> params) {
        Query query = getSession().createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.executeUpdate();
    }

    protected int digitalHqlResult(String hql, Map<String, Object> params) {
        Query query = getSession().createQuery(hql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return ((Number) query.iterate().next()).intValue();
    }

    protected int digitalSqlResult(String sql, Map<String, Object> params) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return ((BigInteger) query.uniqueResult()).intValue();
    }

    //----------------------------------------
    // SQL List
    //----------------------------------------

    @SuppressWarnings("unchecked")
    protected List<T> sqlList(String sql, Map<String, Object> params) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.addEntity(entityClass);
        return query.list();
    }

    //----------------------------------------
    // SQL Query
    //----------------------------------------

    @SuppressWarnings("unchecked")
    protected List<?> sqlQuery(String sql, Map<String, Object> params) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        return query.list();
    }

    @SuppressWarnings("unchecked")
    protected List<?> sqlQuery(String sql, Map<String, Object> params, Class cls) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.addEntity(cls);
        return query.list();
    }

    @SuppressWarnings("unchecked")
    protected List<?> sqlQuery(String sql, Map<String, Object> params, Map<String, Class> classes) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        for (String key : classes.keySet()) {
            query.addEntity(key, classes.get(key));
        }
        return query.list();
    }

    //----------------------------------------
    // Execute update
    //----------------------------------------

    protected void executeSQL(String sql, Map<String, Object> params) {
        SQLQuery query = getSession().createSQLQuery(sql);
        Set<String> paramNames = params.keySet();
        for (String name : paramNames) {
            query.setParameter(name, params.get(name));
        }
        query.executeUpdate();
    }

    protected final Log log = LogFactory.getLog(getClass());
    protected Class<? extends T> entityClass;

}
