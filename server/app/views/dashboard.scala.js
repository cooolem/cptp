define(['jquery',
        'jqueryui'                         // Force jQuery UI to be loaded
    ],
    function($)
    {
        // Calling noConflict(true) forces jQuery to remove all global jQuery
        // variables, namely '$' and 'jQuery', from the global scope. This
        // ensures that jQuery can only be referenced as a module (which is
        // loaded and managed by RequireJS).
        $.noConflict(true);
        $(function() {
            var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket
            var chatSocket = new WS("@routes.Application.chat().webSocketURL(request)");

            var sendMessage = function() {
                chatSocket.send(JSON.stringify(
                    {text: $("#talk").val()}
                ))
                $("#talk").val('')
            }

            //var receiveEvent =  dashboardController;

            var handleReturnKey = function(e) {
                if(e.charCode == 13 || e.keyCode == 13) {
                    e.preventDefault()
                    sendMessage()
                }
            }

            $("#talk").keypress(handleReturnKey)

            chatSocket.onmessage = receiveEvent

        })



    });

