"use strict";

define([
    'jquery',
    'util/base'
],
function($, Base)
{
    // Extend the Base module.
    AxoneWebSocket.extend(Base);

    /**
     * The instantiation function (constructor) for this module.
     */
    function AxoneWebSocket(wsLocation)
    {
        // Initialize the supermodule by calling its constructor function.
        Base.call(this);

        var bef = new Date();

        // Create the correct WebSocket type.
        var WSClass = window['MozWebSocket'] ? MozWebSocket : WebSocket;

        this.webSocket = new WSClass(wsLocation);

        var me = this;

        // Map the events from the internal WebSocket to the events delivered by
        // this websocket class.
        this.webSocket.onopen = function()
        {
            var aft = new Date();
            console.log("open web socket "+(aft.getTime() - bef.getTime())/1000);
            $(me).trigger("open");
        };
        this.webSocket.onmessage = function(message)
        {
            $(me).trigger("message", message);
        };
        this.webSocket.onclose = function()
        {
            $(me).trigger("close");
        };
        this.webSocket.onerror = function()
        {
            $(me).trigger("error");
        };

        this.send = function(message)
        {
            this.webSocket.send(message);
        };

        this.close = function()
        {
            this.webSocket.close();
        };

        return this;
    }

    // Return the constructor function for this module.
    return AxoneWebSocket;
});
