"use strict";

define(function()
{
    /**
     * A utility function used to get the foreground color associated with an
     * alarm priority.
     *
     * @param {Object} alarmPriority to retrieve the foreground color for
     */
    var getAlarmForeground = function(alarmPriority)
    {
        if (alarmPriority == "LOW")
        {
            return "#000000";
        }
        else if (alarmPriority == "MEDIUM")
        {
            return "#000000";
        }
        else if (alarmPriority == "HIGH")
        {
            return "#ffffff";
        }

        return "#FFFFFF";
    }

    return getAlarmForeground;
});
