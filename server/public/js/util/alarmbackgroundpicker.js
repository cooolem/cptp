"use strict";

define(function()
{
    /**
     * A utility function used to get the background color associated with an
     * alarm priority.
     *
     * @param {Object} alarmPriority to retrieve the background color for
     */
    var getAlarmBackground = function(alarmPriority)
    {
        if(alarmPriority == "LOW")
        {
            return "#1ea5ff";
        }
        else if(alarmPriority == "MEDIUM")
        {
            return "#ffff00";
        }
        else if(alarmPriority == "HIGH")
        {
            return "#ff3232";
        }

        return "#000000";
    }

    return getAlarmBackground;
});
