"use strict";

define(function()
{
    /**
     * A utility function used to get the foreground color associated with an
     * alarm level.
     *
     * @param {Object} alarmLevel to retrieve the foreground color for
     */
    var getAlarmForeground = function(alarmLevel)
    {
        switch(alarmLevel)
        {
            default:
            case 0:                     // No alarm
                return "#20c040";
            case 1:                     // Information alarm
                return "#000000";
            case 2:                     // Low alarm
                return "#000000";
            case 3:                     // Medium alarm
                return "#000000";
            case 4:                     // High alarm
                return "#ffffff";
        }
    }

    return getAlarmForeground;
});
