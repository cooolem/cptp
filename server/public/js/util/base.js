"use strict";

/**
 * This is the base module in the module hierarchy. All modules directly
 * or indirectly extend this module.
 *
 * This bass module provides a number of features that are familiar to those
 * used to class-based programming.
 *
 *  o Sub-classing: A module can extend another module, inheriting its data
 *    and behaviors.
 *  o Private module data: Variables defined in the module definition function
 *    will only exist once for all module instances and be visible only within
 *    module instances and the module constructor. These variables will not be
 *    visible outside module instances.
 *  o Private module functions: Functions defined in the module definition
 *    function exist once for all module instances and are visible only within
 *    protected and public instance functions and the module constructor. These
 *    functions will not be visible outside module instances.
 *  o Protected instance functions: Functions defined in the module constructor
 *    function are visible to other protected and public instance functions but
 *    are not visible outside module instances.
 *  o Public instance data: Variables added to the module constructor prototype
 *    that have unique values in all module instances. These variables are
 *    publically visible (readable and writable) and visible to sub-modules.
 *  o Public instance functions: Functions added to the module prototype that
 *    are publically callable and inherited on sub-modules.
 *
 * Below is an example of a module that demonstrates many of the features
 * described above.
 *
 *  define(['basemodule'], function(BaseModule) {
 *
 *      // Extend the base module.
 *      DerivedModule.extend(BaseModule);
 *
 *      // The module constructor.
 *      function DerivedModule() {
 *          // Initialize the base module by calling its constructor function.
 *          BaseModule.apply(this);
 *
 *          // A protected instance function.
 *          this.protectedFun = function(){
 *              // ..
 *          };
 *      };
 *
 *      // A private module variable.
 *      var privateVar = "only one and im private";
 *
 *      // A private module function.
 *      var privateFun = function() {
 *          // ...
 *          this.protectedFun = function() {
 *              // ...
 *          };
 *      };
 *      // A public instance variable.
 *      DerivedModule.prototype.publicVar = "one per instance and im public";
 *
 *      // A public instance function.
 *      DerivedModule.prototype.publicFun = function() {
 *          // ...
 *      };
 *
 *      // Must return the constructor.
 *      return DerivedModule;
 *  };
 */
define(function()
{
    function Base()
    {
        // Add any values or functions that are to be common to all
        // of the modules we define, but private outside the modules.

        return this;
    };

    /**
     * Get the name of this module. This function uses the module constructor
     * function property to determine the name which, by convention, is the
     * module name.
     *
     * @return the name of the module
     */
    Base.prototype.getModuleName = function()
    {
        // The name property of a Function object is not universally supported.
        // Workaround is to extract it from the string representation of the
        // Function object.
        return this.constructor.name ||
               this.constructor.toString().match(/function\s*([^(]*)\(/)[1];
    };

    /**
     * A function to aid in module extension. This function creates a prototype
     * and sets it on the caller. This new prototype has as its prototype the
     * prototype of the constructor function of the module being extended. It
     * then makes sure the caller's constructor is set correcly on the
     * prototype.
     *
     * @param baseModuleConstructor constructor of the module being sub-classed
     */
    Function.prototype.extend = function(baseModuleConstructor)
    {
        this.prototype = Object.create(baseModuleConstructor.prototype);
        this.prototype.constructor = this;
    };

    // Return the constructor function for this module.
    return Base;
});
