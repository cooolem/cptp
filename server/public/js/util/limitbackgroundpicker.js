"use strict";

define(function()
{
    /**
     * A utility function used to get the background color associated with an
     * alarm level.
     *
     * @param {Object} alarmLevel to retrieve the background color for
     */
    var getLimitBackgroundColor = function(alarmLevel)
    {
        switch(alarmLevel)
        {
            default:
            case 0:                     // No alarm
                return "#000000";
            case 1:                     // Information alarm
                return "#aaaaaa";
            case 2:                     // Low alarm
                return "#1ea5ff";
            case 3:                     // Medium alarm
                return "#ffff00";
            case 4:                     // High alarm
                return "#ff3232";
        }
    }

    return getLimitBackgroundColor;
});
