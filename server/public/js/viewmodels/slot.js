"use strict";

define([
    'jquery',
    'models/model',
    'viewmodels/titlebar',
    'viewmodels/numericblock',
    'models/waveformmodel',
    'renderers/waveformrenderer'
],
function($, Model, TitleBar, NumericBlock, WaveformModel, WaveformRenderer)
{
    // Extend the Model module.
    Slot.extend(Model);

    // The valid fields for which this model maintains values.
    var fields =
    [
        { name: 'id' },
        { name: 'titleBar' },
        { name: 'numeric1' },
        { name: 'numeric2' },
        { name: 'waveformRenderer' },
        { name: 'waveformModels' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, Slot);

    /**
     * The instantiation function (constructor) for this module.
     */
    function Slot(patientDesc)
    {
        var initializer = {
            id: patientDesc.id,
            titleBar: new TitleBar(patientDesc),
            numeric1: new NumericBlock(),
            numeric2: new NumericBlock(),
            waveformRenderer: undefined,
            waveformModels: []
        }

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        return this;
    }

    /**
     * Update the title bar information for this slot.
     *
     * @param descriptor the descriptor holding updated patient properties
     */
    Slot.prototype.updateTitleBar = function(descriptor)
    {
        this.getTitleBar().update(descriptor);
    }

    /**
     * Update the numeric instance whose ID is given in the specified descriptor
     * instance. If a numeric with the ID in the descriptor is not found then a
     * new instance is created.
     *
     * @param descriptor the descriptor holding updated numeric properties
     */
    Slot.prototype.addOrUpdateNumeric = function(descriptor)
    {
        var numeric = this.findNumeric(descriptor.id);

        // Create a numeric since one does not yet exist.
        if (numeric == undefined)
        {
            // TODO - currently just setting to numeric1 by default
            numeric = this.getNumeric1();
        }

        numeric.updateNumeric(descriptor);

        // TODO just for testing now
        this.getTitleBar().updateAlarm(descriptor.value);

        switch(descriptor.value % 10)
        {
        case 0:
            this.getTitleBar().update({
                firstName: "Tyler",
                lastName: "Craig",
                location: "ICU"
            });
            break;
        case 1:
            this.getTitleBar().update({
                firstName: "Lisa",
                lastName: "Craig",
                location: "ER"
            });
            break;
        case 2:
            this.getTitleBar().update({
                firstName: "Mary",
                lastName: "James",
                location: "RM2"
            });
            break;
        default:
            break;
        }
    }

    /**
     * Update the waveform instance whose ID is given in the specified descriptor
     * instance. If a waveform with the ID in the descriptor is not found then a
     * new instance is created.
     *
     * @param descriptor the descriptor holding updated waveform properties
     */
    Slot.prototype.addOrUpdateWaveform = function(descriptor)
    {
        var waveform = this.findWaveform(descriptor.id);

        if (waveform == undefined)
        {
            waveform = new WaveformModel(descriptor);
            this.getWaveformModels().push(waveform);

            // This is done here because it has to get 2D context.
            if (this.getWaveformRenderer() == undefined)
            {
                this.setWaveformRenderer(new WaveformRenderer(this.getId(), manageCanvasSize));
            }

            this.getWaveformRenderer().addWaveform(waveform);
        }

        waveform.update(descriptor);
    }

    Slot.prototype.findNumeric = function(numericID)
    {
        if (numericID === this.getNumeric1().getId())
        {
            return this.getNumeric1();
        }
        else if (numericID === this.getNumeric2().getId())
        {
            return this.getNumeric2();
        }

        return undefined;
    }

    Slot.prototype.findWaveform = function(waveformID)
    {
        var myWaveform = undefined;

        $.each(this.getWaveformModels(), function(index, waveform)
        {
            if (waveformID === waveform.getId())
            {
                myWaveform = waveform;
                return false; // stop iterating
            }
        });

        return myWaveform;
    }

    Slot.prototype.toString = function()
    {
        return "Slot[id=" + this.getId() + ", " +
               "lastName=" + this.getTitleBar().getLastName() + ", " +
               "firstName=" + this.getTitleBar().getFirstName() + ", " +
               "location=" + this.getTitleBar().getLocation() +
               "]";
    }

    /**
     * Handle resizing of the waveform canvas, as necessary. It will be
     * necessary to adjust the canvas size if this slot is resized.
     *
     * @param wfRenderer the waveform renderer whose canvas size is to be managed
     */
    var manageCanvasSize = function(wfRenderer)
    {
        var slotId = wfRenderer.getSlotId();

        var slotWidth      = $("#" + slotId).width();
        var numBlocksWidth = $("#" + slotId + " > .numericBlocks").width();

        var newWidth = slotWidth - numBlocksWidth;

        var canvasEl = wfRenderer.getCanvas();

        // Make sure not to assign the width if it has not changed since the
        // assignment will clear the canvas.
        if (newWidth != canvasEl.width)
        {
            canvasEl.width = newWidth;
        }
    }

    // Return the constructor function for this module.
    return Slot;
});
