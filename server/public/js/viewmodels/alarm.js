"use strict";

define([
    'ko',
    'models/model',
    'util/alarmforegroundpicker',
    'util/alarmbackgroundpicker'
],
function(ko, Model, getAlarmForeground, getAlarmBackground)
{
    // Extend the Model module.
    Alarm.extend(Model);

    // The valid fields for which this model maintains values.
    var fields =
    [
        { name: 'alarmPriority', type: 'observable' },
        { name: 'alarmText',     type: 'observable' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, Alarm);

    /**
     * The instantiation function (constructor) for this module.
     */
    function Alarm()
    {
        var initializer =
        {
            alarmPriority: "",
            alarmText: ""
        }

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        // Additional getters used to set style in UI.
        var self = this;

        this.getAlarmForeground = ko.computed(function()
        {
            return getAlarmForeground(self.getAlarmPriority());
        });

        this.getAlarmBackground = ko.computed(function()
        {
            return getAlarmBackground(self.getAlarmPriority());
        });

        return this;
    }

    Alarm.prototype.updateAlarm = function(val)
    {
        switch(val%10)
        {
            case 1:
                this.setAlarmText('ASYSTOLE');
                this.setAlarmPriority('HIGH');
                break;
            case 4:
                this.setAlarmText('VTAC/VFIB');
                this.setAlarmPriority('MEDIUM');
                break;
            case 6:
                this.setAlarmText('ARTIFACT');
                this.setAlarmPriority('LOW');
                break;
            default:
                this.setAlarmText('');
                this.setAlarmPriority('');
        }
    }

    // Return the constructor function for this module.
    return Alarm;
});