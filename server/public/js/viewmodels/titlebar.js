"use strict";

define([
    'ko',
    'models/model',
    'viewmodels/alarm'
],
function(ko, Model, Alarm)
{
    // Extend the Model module.
    TitleBar.extend(Model);

    // The valid fields for which this model maintains values.
    var fields =
    [
        { name: 'firstName',    type: 'observable' },
        { name: 'lastName',     type: 'observable' },
        { name: 'location',     type: 'observable' },
        { name: 'alarm',        type: 'plain' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, TitleBar);

    /**
     * The instantiation function (constructor) for this module.
     */
    function TitleBar(descriptor)
    {
        var initializer = {
            location: descriptor.location,
            firstName: descriptor.firstName,
            lastName: descriptor.lastName,
            alarm: new Alarm()
        }

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        var self = this;
        this.getPatientName = ko.computed(function()
        {
            return self.getFirstName() + " " + self.getLastName();
        });

        return this;
    }

    TitleBar.prototype.update = function(descriptor)
    {
        this.setFirstName(descriptor.firstName);
        this.setLastName(descriptor.lastName);
        this.setLocation(descriptor.location);
    }


    TitleBar.prototype.updateAlarm = function(val)
    {
        this.getAlarm().updateAlarm(val);
    }

    // Return the constructor function for this module.
    return TitleBar;
});
