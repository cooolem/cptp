"use strict";

define([
    'jquery',
    'models/model',
    'viewmodels/slot'
],
function($, Model, Slot)
{
    // Extend the Model module.
    PatientSlots.extend(Model);

    // The valid fields for which this model maintains values.
    var fields =
    [
        { name: 'slots', type: 'observableArray' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, PatientSlots);

    /**
     * The instantiation function (constructor) for this module.
     */
    function PatientSlots(patientDesc)
    {
        var initializer = {
            slots: [],
        }

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        return this;
    }

    /**
     * Update the patient instance whose ID is given in the specified
     * descriptor instance. If a patient with the ID in the descriptor
     * is not found then a new instance is created.
     *
     * @param descriptor the descriptor holding updated patient properties
     */
    PatientSlots.prototype.dashboard = function(descriptor)
    {
        var slot = this.findSlot(descriptor.id);

        if (slot == undefined)
        {
            slot = new Slot(descriptor);
            this.getSlots().push(slot);

            console.log(this.getModuleName() + ": Added " + slot);
        }
        else
        {
            slot.updateTitleBar(descriptor);
        }
    }

    // Return the constructor function for this module.
    return PatientSlots;
});
