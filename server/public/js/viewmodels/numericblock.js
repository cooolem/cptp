"use strict";

define([
    'ko',
    'models/model',
    'util/limitbackgroundpicker',
    'util/limitforegroundpicker'
],
function(ko, Model, getLimitBackground, getLimitForeground)
{
    // Extend the Model module.
    NumericBlock.extend(Model);

    // The valid fields for which this model maintains values.
    var fields =
    [
        { name: 'id',           type: 'observable' },
        { name: 'parameter',    type: 'observable' },
        { name: 'label',        type: 'observable' },
        { name: 'uoM',          type: 'observable' },
        { name: 'lowerLimit',   type: 'observable' },
        { name: 'upperLimit',   type: 'observable' },
        { name: 'value',        type: 'observable' },
        { name: 'alarmLimit',   type: 'observable' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, NumericBlock);

    /**
     * The instantiation function (constructor) for this module.
     */
    function NumericBlock()
    {
        var initializer = {
            id: "",
            parameter: "",
            label: "",
            uoM: "",
            lowerLimit: "",
            upperLimit: "",
            value: "",
            alarmLimit: ""
        }

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        var self = this;

        // additional getters used to set style in UI
        this.getLimitBackground = ko.computed(function()
        {
            return getLimitBackground(self.getAlarmLimit());
        });

        this.getLimitForeground = ko.computed(function() {
            return getLimitForeground(self.getAlarmLimit());
        });

        return this;
    }

    /**
     * Update the numeric instance whose ID is given in the specified descriptor
     * instance.
     *
     * @param descriptor the descriptor holding updated numeric properties
     */
    NumericBlock.prototype.updateNumeric = function(descriptor)
    {
        this.setId(descriptor.id);
        this.setParameter(descriptor.parameter);
        this.setLabel(descriptor.label);
        this.setUoM(descriptor.uoM);
        this.setLowerLimit(descriptor.lowerLimit);
        this.setUpperLimit(descriptor.upperLimit);
        this.setValue(descriptor.value);
        this.setAlarmLimit(descriptor.value%15);
    }

    // Return the constructor function for this module.
    return NumericBlock;
});
