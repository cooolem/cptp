"use strict";

define([
    'jquery',
    'util/base'
],
function($, Base)
{
    // Extend the Base module.
    DataController.extend(Base);

    /**
     * Initialize the controller instance. Initialization includes adding
     * itself as a listener for patient, waveform and numeric update events.
     */
    function DataController(application, dashboard)
    {
        // Initialize the supermodule by calling its constructor function.
        Base.call(this);

        this.dashboard = dashboard;

        var self = this;

        // Register the handler of patient descriptor events from the application.
        $(application).bind("dashboard", self, function(event, dashboard)
        {
            self.dashboard(dashboard);
        });
    }

    /**
     * Add or update the patient instance whose ID is given in the specified
     * descriptor instance.
     *
     * @param descriptor the descriptor holding updated patient properties
     */
    DataController.prototype.dashboard = function(dashboard)
    {
        if (descriptor.id != undefined)
        {
            this.dashboard.dashboard(dashboard);
        }
        else
        {
            console.log(this.getModuleName() + ".patientUpdate: received descriptor with no ID");
        }
    }





    // Return the constructor function for this module.
    return DataController;
});
