"use strict";

define([
    'jquery',
    'util/base',
    'util/axonewebsocket'
],
function($, Base, AxoneWebSocket)
{
    // Extend the Base module.
    CommController.extend(Base);

     /**
     * Initialize this instance. This function creates and adds itself as a
     * listener to a new websocket.
     */
    function CommController(application)
    {
        // Initialize the supermodule by calling its constructor function.
        Base.call(this);

        var self = this;

        var beforeAjax = new Date();

        var num = 0;

        function openWebSocket(data)
        {
            var webSocket = new AxoneWebSocket(rtWSLocation);

            // listeners
            $(webSocket).bind("open", function(event)
            {
                //console.profile();
                self.handleOpen(webSocket);
            });

            $(webSocket).bind("close", function(event)
            {
                self.handleClose(webSocket);
            });

            $(webSocket).bind("message", self, function(event, message)
            {
                if(num < 1000)
                {
                    num++;
                }
                if(num == 1000)
                {
                   // console.profileEnd();
                    num++
                }
                self.handleMessage(message);
            });
        }

        /**
         * Handle the websocket open event. When opened, this function will send a
         * start message to the server over the websocket.
         */
        this.handleOpen = function(webSocket)
        {
            webSocket.send(JSON.stringify("start"));
            console.log(this.getModuleName() + ": Websocket opened, URL: " + rtWSLocation);
        }

        /**
         * Handle the websocket close event. No application action is required.
         */
        this.handleClose = function(webSocket)
        {
            webSocket.close();
            console.log(this.getModuleName() + ": Websocket closed");
        }

        /**
         * Handle the websocket message received event. This function will,
         * based on the message type, generate patient, waveform, and numeric
         * received events to the application.
         *
         *  @param message the received message being notified by the event
         */
        this.handleMessage = function(message)
        {
            if(message.data)
            {
                // Parse the received JSON into an object.
                var dataStream = JSON.parse(message.data);
                $(application).trigger("dashboard", dataStream);
            }
        }
    }

    // Return the constructor function for this module.
    return CommController;
});
