"use strict";

define([
    'models/model'
],
function(Model)
{
    // Extend the Model module.
    Queue.extend(Model);

    // The valid fields for which this model maintains values.
    var fields =
    [
        { name: 'offset' },
        { name: 'qArray' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, Queue);

    function Queue(descriptor)
    {
        var initializer = {
            offset: 0,
            qArray: []
        }

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        return this;
    }


    /** Returns the length of the queue. */
    Queue.prototype.getLength = function()
    {
        return (this.getQArray().length - this.getOffset());
    }

    /** Returns true if the queue is empty, and false otherwise. */
    Queue.prototype.isEmpty = function()
    {
        return (this.getQArray().length == 0);
    }

    /**
     * Enqueues the specified item. The parameter is:
     *
     * @param item the item to enqueue
     */
    Queue.prototype.enqueue = function(item)
    {
        // enqueue the item
        this.getQArray().push(item);
    }

    /**
     * Dequeues an item and returns it. If the queue is empty then undefined is
     * returned.
     */
    Queue.prototype.dequeue = function()
    {
        // if the queue is empty, return undefined
        if (this.getQArray().length == 0)
        {
            return undefined;
        }

        // store the item at the front of the queue
        var item = this.getQArray()[this.getOffset()];

        // increment the offset and remove the free space if necessary
        this.setOffset(this.getOffset()+1);

        if (this.getOffset() * 2 >= this.getQArray().length)
        {
            this.setQArray(this.getQArray().slice(this.getOffset()));
            this.setOffset(0);
        }

        // return the dequeued item
        return item;
    }

    /**
     * Returns the item at the front of the queue (without dequeuing it). If the
     * queue is empty then undefined is returned.
     */
    Queue.prototype.peek = function()
    {
        // return the item at the front of the queue
        return (this.getQArray().length > 0 ? this.getQArray()[this.getOffset()] : undefined);
    },

    /**
     * Empties the queue.
     */
    Queue.prototype.clear = function()
    {
        this.getQArray().splice(0, this.getQArray().length);
    }

    return Queue;
});
