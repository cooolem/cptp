"use strict";

define([
    'jquery',
    'models/model',
    'models/queue'
],
function($, Model, Queue)
{
    // Extend the Model module.
    WaveformModel.extend(Model);

    // The valid fields for which this model maintains values.
    var fields = [
        { name: 'id' },
        { name: 'parentID' },
        { name: 'resolution' },
        { name: 'sampleRate' },
        { name: 'label' },
        { name: 'samplePoints' }
    ];

    // Initialize this model for the fields defined.
    Model.prototype.initializeFields(fields, WaveformModel);

    function WaveformModel(descriptor)
    {
        var initializer = {
            id: descriptor.id,
            parentID: descriptor.parentID,
            resolution: descriptor.resolution,
            sampleRate: descriptor.sampleRate,
            label: descriptor.label,
            samplePoints: new Queue()
        };

        // Initialize the supermodule by calling its constructor function.
        Model.call(this);

        // Initialize the field values of this model.
        this.initializeFieldValues(initializer, fields);

        return this;
    }

    /**
     * Update this waveform instance with the data in the specified descriptor.
     *
     * @param descriptor container for properties to merge into this waveform
     */
    WaveformModel.prototype.update = function(descriptor)
    {
        var me = this;
        // Set the field values that are specified in the descriptor.
        // (just doing at beginning right now)

        // Block events being delivered to the store. TODO

        // Load the waveform sample points into the waveform.
        $.each(descriptor.waveformPoints, function(index, item) {
            me.getSamplePoints().enqueue(new Array(item.s, item.y));
        });

        // Let listeners know that this model was updated.
        //this.fireEvent('update', this);
    }

    /**
     * Get the length of waveform sample points in the queue.
     */
    WaveformModel.prototype.getSamplesLength = function()
    {
        return this.getSamplePoints().getLength();
    }

    /**
     * Peek, without removal, some incoming waveform sample points to the queue.
     */
    WaveformModel.prototype.peekSample = function()
    {
        return this.getSamplePoints().peek();
    }

    /**
     * Get an incoming waveform sample point from the queue.
     */
    WaveformModel.prototype.dequeueSample = function()
    {
        return this.getSamplePoints().dequeue();
    }

    // Return the constructor function for this module.
    return WaveformModel;
});
