"use strict";

/**
 * The definition of a model module. All models must directly or indirectly
 * extend this module.
 *
 * Models allow the fields that hold the model information to be easily
 * declared. This model base module will automatically create a getter and setter
 * for each model field, thus eliminating the need to explicitly write these.
 *
 * The fields that hold the model information are declared as entries in a
 * module definition function variable named "fields". The fields variable
 * must be an array of objects, each object consisting of the name of the field.
 * Optional information can also be specified for each field, including its
 * knockout type (observable, observable array) and whether or not the field is
 * writable. A conversion function can also be specified. This function takes
 * the value specified to the field setter function each time it is called
 * and returns the value that is then actually set on the field.
 *
 * Below is an example of the declaration of the fields of a model that includes
 * a person's first and last names, social security number, and date of birth.
 * The first name, last name, and social security number fields are observable for
 * the purposes of knockout. Additionally, the social security number is read-
 * only (it will not receive a setter function). The date of birth specifies a
 * conversion function that takes an ISO 8601 date string and converts it to a
 * JavaScript Date instance.
 *
 *    var fields = [
 *        { name: "firstName",  type: 'obsv' },
 *        { name: "lastName",   type: 'obsv' },
 *        { name: "ssn",        type: 'obsv', writable: false },
 *        { name: 'dateOfBirth',
 *          // Convert ISO 8601 date string (yyyy-mm-dd)) to a Date instance.
 *          convert: function(val) {
 *              return new Date(val);
 *          }
 *        }
 *    ];
 */
define([
    'ko',
    'util/base'
],
function(ko, Base)
{
    // Extend the Base module.
    Model.extend(Base);

    //
    // Private module variables.
    //

    /** The default field type. */
    var FT_PLAIN = "plain";
    /** The field type for a knockout observable. */
    var FT_OBSERVABLE = "observable";
    /** The field type for a knockout observable array. */
    var FT_OBSERVABLE_ARRAY = "observableArray";

    /**
     * The instantiation function (constructor) for a model.
     */
    function Model()
    {
        // Initialize the supermodule by calling its constructor function.
        Base.apply(this);

        //
        // Private instance variables.
        //

        // The private field values managed by this model instance.
        var _fieldValues = {};

        /**
         * Get the value of the named field.
         *
         * @param fieldName the name of the field
         * @param fieldType the type of the field
         */
        this.get = function(fieldName, fieldType)
        {
            // Observable arrays are not returned as observables since they
            // have array methods of their own.
            if (fieldType === FT_OBSERVABLE)
            {
                return _fieldValues[fieldName]();
            }

            return _fieldValues[fieldName];
        };

        /**
         * Set the value of the named field.
         *
         * @param fieldName the name of the field whose value is to be set
         * @param value the new value of the field
         * @param ignoreObservable ignore the fact that a field is observable
         */
        this.set = function(fieldName, value, ignoreObservable)
        {
            set(fieldName, value, _fieldValues, this, ignoreObservable);
        };

        return this;
    };

    //
    // Public functions (added to constructor prototype).
    //

    /**
     * Initialize the fields that this model maintains values. This
     * includes adding accessor functions for the specified fields to the
     * specified constructor's prototype object, as well as saving the
     * descriptors themselves for later use.
     */
    Model.prototype.initializeFields = function (fieldDescs, ctor)
    {
        var proto = ctor.prototype;

        // Add a getter and setter for each field in the field descriptors.
        for (var i in fieldDescs)
        {
            var fieldDesc = fieldDescs[i];

            // Make sure the descriptor is valid.
            validateFieldDesc(fieldDesc, proto);

            // Supply the default field type if one was not specified.
            if (fieldDesc.type == undefined)
            {
                fieldDesc.type = FT_PLAIN;
            }

            addGetterFunction(fieldDesc.name, fieldDesc.type, proto);

            // Read-only fields (writable false) don't get a setter.
            if (fieldDesc.writable !== false)
            {
                addSetterFunction(fieldDesc.name, fieldDesc.type, proto);
            }
        }

        // Add a locked-down descriptor container in the contructor's prototype.
        Object.defineProperty(proto, "_fieldDescriptors",
                              {value: {}, writable: false, enumerable: false, configurable: false});

        // Save the descriptors to the contructor's prototype.
        addAll(fieldDescs, proto._fieldDescriptors);
    };

    /**
     * Initialize the fields of the model with the values in the initializer.
     * The fields are name-value pairs (object properties). The specified
     * initializer contains the properties that the caller wants incorporated
     * into the fields of this model. The specified field descriptors provide
     * the list of fields supported by this model.
     *
     * Only fields that are present in both the specified field descriptors
     * and the initializer are set on the model; others are ignored.
     */
    Model.prototype.initializeFieldValues = function(initializer, fieldDescArray)
    {
        // Load each of the properties from the initializer into the fields
        // of this model instance.
        for (var fieldName in initializer)
        {
            var fieldDesc = findFieldDescriptor(fieldName, fieldDescArray);

            // Only add if the named field's descriptor is found.
            if (fieldDesc)
            {
                // Must set knockout observable and observable array normally
                // the first time, thus pass true to ignore the observable.
                if (fieldDesc.type === FT_OBSERVABLE)
                {
                    this.set(fieldName, ko.observable(initializer[fieldName]), true);
                }
                else if (fieldDesc.type === FT_OBSERVABLE_ARRAY)
                {
                    this.set(fieldName, ko.observableArray(initializer[fieldName]), true);
                }
                else
                {
                    this.set(fieldName, initializer[fieldName], false);
                }
            }
        }
    };

    /**
     * Create and return a string representation of this model.
     */
    Model.prototype.toString = function()
    {
        // Get all the descriptors for this model.
        var fieldDescs = collectAllFieldDescriptors(Object.getPrototypeOf(this), {});

        var fieldsStr = "";

        // Create a string including each of the model's fields.
        for (var d in fieldDescs)
        {
            var fieldValue = this.get(fieldDescs[d].name);

            if (fieldValue !== undefined)
            {
                fieldsStr += (fieldsStr ? "; " : "") + d + ": " + fieldValue;
            }
        }

        return this.getModuleName() + "[" + fieldsStr + "]";
    };


    //
    // Private functions (declared in module definition function).
    //

    /**
     * Validate that the specified field descriptor is valid. Throw an error
     * if not.
     *
     * @param fieldDesc the field descriptor to validate
     * @param proto the prototype of the model instance that owns the field
     */
    var validateFieldDesc = function(fieldDesc, proto)
    {
        // The field must have a name.
        if (fieldDesc.name === undefined)
        {
            throw new Error("Model \"" + proto.getModuleName() +
                            "\"; The name of the model field has not been specified");
        }

        // Make sure the field type is valid.
        if (fieldDesc.type !== undefined            &&
            (fieldDesc.type !== FT_PLAIN            &&
             fieldDesc.type !== FT_OBSERVABLE       &&
             fieldDesc.type !== FT_OBSERVABLE_ARRAY))
        {
            throw new Error("Model \"" + proto.getModuleName() +
                            "\"; The type of model field \"" + fieldDesc.name +
                            "\" is unknown: \"" + fieldDesc.type + "\"");
        }

        // Make sure that the writable value is a boolean.
        if (fieldDesc.writable !== undefined && (typeof fieldDesc.writable !== "boolean"))
        {
            throw new Error("Model \"" + proto.getModuleName() +
                            "\"; The writable property specified for model field \"" + fieldDesc.name +
                            "\" is not a boolean: \"" +
                            fieldDesc.writable + "\"");
        }

        // Make sure that the convert value is a function.
        if (fieldDesc.convert !== undefined && (typeof fieldDesc.convert) !== "function")
        {
            throw new Error("Model \"" + proto.getModuleName() +
                            "\"; The conversion specified is not a function");
        }
    };

    /**
     * Set the value of a field on the specified Model instance.
     *
     * @param fieldName the name of the field whose value is to be set
     * @param value the value to set on the field
     * @param fieldValues the current values of the fields of the model instance
     * @param instance the instance of the model whose field value is to be set
     * @param ignoreObservable ignore the fact that a field is observable
     */
    var set = function(fieldName, value, fieldValues, instance, ignoreObservable)
    {
        var fieldDesc = getFieldDescriptor(fieldName, Object.getPrototypeOf(instance));

        // Make sure there is a descriptor for this field.
        if (fieldDesc)
        {
            // Apply the field value conversion function, if one was specified.
            var fieldValue = typeof fieldDesc.convert === "function" ?
                             fieldDesc.convert(value) : value;

            // Only set the field if the value is defined.
            if (fieldValue !== undefined)
            {
                var fieldWritable = !(fieldDesc.writable === false);
                var alreadyDefined = fieldName in fieldValues;

                if (fieldWritable || !alreadyDefined)
                {
                    // Special handling for knowckout observables.
                    if (ignoreObservable != true &&
                        (fieldDesc.type === FT_OBSERVABLE || fieldDesc.type === FT_OBSERVABLE_ARRAY))
                    {
                        fieldValues[fieldName](fieldValue);
                    }
                    else
                    {
                        fieldValues[fieldName] = fieldValue;
                    }
                }
            }
        }
        else
        {
            throw new Error("Model has no field \'" + fieldName + "\': " + instance);
        }
    };

    /**
     * Add getter function for the specified field. The function is added
     * to the model prototype.
     *
     * @param fieldName the name of the field retrieved by the getter
     * @param the type of the field
     * @param proto the prototype of the model instance that owns the field
     */
    var addGetterFunction = function(fieldName, fieldType, proto)
    {
        // Add the getter to the prototype.
        proto[formAccessorName("get", fieldName)] = function()
        {
            return this.get(fieldName, fieldType);
        };
    };

    /**
     * Add setter function for the specified field. The function is added
     * to the model prototype.
     *
     * @param fieldName the name of the field set by the setter
     * @param the type of the field
     * @param proto the prototype of the model instance that owns the field
     */
    var addSetterFunction = function(fieldName, fieldType, proto)
    {
        // Add the setter to the prototype.
        proto[formAccessorName("set", fieldName)] = function(value)
        {
            return this.set(fieldName, value, fieldType);
        };
    };

    /**
     * Form the name of an accessor function. The name is a concatenation of the
     * specified accessor name and the capitalized field name, also specified.
     */
    var formAccessorName = function(accessor, fieldName)
    {
        return accessor +
               fieldName.replace(new RegExp( "^[a-z]", "" ),
                                 function($0) { return($0.toUpperCase()); });
    };

    /**
     * Find the field descriptor for the named field from the specified array
     * of field descriptors.
     */
    var findFieldDescriptor = function(fieldName, fieldDescArray)
    {
        // Look for the desired descriptor in the simple array.
        for (var i in fieldDescArray)
        {
            if (fieldName === fieldDescArray[i].name)
            {
                return fieldDescArray[i];
            }
        }

        return undefined;
    };

    /**
     * Retrieve the field descriptor for the named field from the specified
     * prototype. If the prototype does not contain the field, the next
     * prototype in the prototype chain is search. This continues until
     * the field is found or the null prototype is reached.
     */
    var getFieldDescriptor = function(fieldName, proto)
    {
        if (proto && proto._fieldDescriptors)
        {
            var desc = proto._fieldDescriptors[fieldName];

            if (desc)
            {
                return desc;
            }
            else
            {
                return getFieldDescriptor(fieldName, Object.getPrototypeOf(proto));
            }
        }

        return undefined;
    };

    /**
     * Collect all the model field descriptors starting at the specified
     * prototype. Descriptors of the next prototype in the prototype chain
     * are also collected. This continues until the null prototype is reached.
     */
    var collectAllFieldDescriptors = function(proto, fieldDescs)
    {
        if (proto != undefined && proto._fieldDescriptors != undefined)
        {
            addAll(proto._fieldDescriptors, fieldDescs);
            return collectAllFieldDescriptors(Object.getPrototypeOf(proto), fieldDescs);
        }

        return fieldDescs;
    };

    /**
     * Add all the source object properties into the destination object.
     */
    var addAll = function(src, dst)
    {
        for (var i in src)
        {
            if (src.hasOwnProperty(i))
            {
                dst[src[i].name] = src[i];
            }
        }
    };

    // Return the constructor function for this module.
    return Model;
});
