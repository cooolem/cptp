requirejs.config(
    {
        baseUrl : "/assets/js",
        // Define paths that are version-dependent here so that these dependencies
        // are all only specified, and need be changed, in this one place.
        paths:
        {
            jquery:                 'vendor/jquery-2.0.0.min',
            angular:                'vendor/angular-1.2.9.min',
            ko:                      'vendor/knockout-2.2.1',
            bootstrap:              'vendor/bootstrap-3.0.3.min',
            flatUi:                  'vendor/flat-ui',
            unslider:                'models/unslider',
            application:             'vendor/application',
            jqueryui:                'vendor/jquery-ui-1.10.2.custom.min',
        },

        shim:
        {
            // Declare that the jQuery UI depends on jQuery. This ensures that
            // jQuery will be loaded before jQuery UI.
            jqueryui: [ 'jquery' ],

            // Since Angular doesn't natively support AMD, we need to shim it.
            angular:
            {
                exports: 'angular'
            }
        }
    });


