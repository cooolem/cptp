define(['jquery',
        'angular',                          // Force jQuery UI to be loaded
        'bootstrap',
        'application',
        'flatUi'
    ],
    function($,ng)
    {
        // Calling noConflict(true) forces jQuery to remove all global jQuery
        // variables, namely '$' and 'jQuery', from the global scope. This
        // ensures that jQuery can only be referenced as a module (which is
        // loaded and managed by RequireJS).
        $.noConflict(true);



    });
