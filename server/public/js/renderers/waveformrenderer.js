"use strict";

define([
    'jquery',
    'util/base'
],
function($, Base)
{
    // Extend the Base module.
    WaveformRenderer.extend(Base);

    //var SCROLLTYPE = {
    //  EBAR : 0,
    //  SCROLL: 1,
    //};

    var COLORS = {
      BLACK:  "#000000",
      RED:    "#F02020",
      GREEN:  "#20C040",
      BLUE:   "#0030F0",
      ORANGE: "#F09000",
    };
    var WFConfig = {
      pxPerMM: 3.8,   // todo: Need real screen calibration values.
      xScale: 25.0,   // mm/s
      yScale: 10.0,   // mm/mv
    //  scrollType: SCROLLTYPE.EBAR,
      EBarWidth: 5,   // mm
      wfColor: [
          COLORS.GREEN,
          "#c08020",
      ],
    };


    var instanceList = new Array();

    var tmLast = 0;

    var intervalUpdate = function() {
        var tmBeg = new Date(); // use Date instead of performance.now() to get to work on ipad
        var tmElapsed = tmBeg - tmLast;
        tmLast = tmBeg;

        // Call WaveformRenderer.update() for each instance.
        for (var i = 0; i < instanceList.length; i++)
            instanceList[i].update(tmElapsed);

        /*Axone.view.renderer.WaveformRenderer.perf.tmAccum += new Date() - tmBeg;  // use Date instead of performance.now() to get to work on ipad
        var tmTotal = tmBeg - Axone.view.renderer.WaveformRenderer.perf.tmMark;
        if (tmTotal >= 5000) {
            Axone.view.renderer.WaveformRenderer.perfOut (tmTotal, Axone.view.renderer.WaveformRenderer.perf.tmAccum);
            Axone.view.renderer.WaveformRenderer.perf.tmAccum = 0;
            Axone.view.renderer.WaveformRenderer.perf.tmMark = tmBeg;
        }*/
    }

    /**
     * Construct a waveform renderer.
     *
     * @param id the ID of the slot holding the canvas onto which waveforms
     * are to be drawn
     * @param intervalCB a callback function that will be called each time the
     * waveform update interval fires, if specified (the callback is optional)
     */
    function WaveformRenderer(id, intervalCB) {

        // Initialize the supermodule by calling its constructor function.
        Base.call(this);

        this.slotId = id;
        this.intervalCallback = intervalCB;

        // get ahold of canvas
        this.canvas = $("#"+id+" > canvas")[0];
        this.context = this.canvas.getContext("2d");

        /** The waveforms to be drawn. */
        this.wfAttribs;
        this.waveforms = new Array();

        this.msInterval = 55;
        this.cResync = 1000 / this.msInterval;  // # of update calls that equal (approx) 1 second of time.
        this.tmStart = new Date();

        this.count = 0;
        this.tmTotal = 0;
        this.bufTotal = 0;
        this.tmBias = 0;
        this.EBarPos = 0;

        instanceList.push(this);
//        console.log ("instanceList: "+Axone.view.renderer.WaveformRenderer.instanceList.length);
        // If this is the first instance created, init interval update.
        if (instanceList.length == 1) {
            setInterval(intervalUpdate, this.msInterval);
        }

        /**
         * Add a waveform to be rendered by this renderer.
         *
         * @param wf the waveform being added
         */
        this.addWaveform = function(wf) {

            this.waveforms.push(wf);

            // Listen for updates to the waveforms.
            //TODO wf.addListener('update', this.handleWFConfigUpdate, this);

            this.wfAttribs = this.calcWFAttribs(this.waveforms);
        }

        /** TODO add this back eventually
         * Handle updates of the specified waveform.
         *
         * @param wf the waveform that was updated
         *
        handleWFConfigUpdate: function(wf)
        {
            var len = this.waveforms[0].getSamplesLength();
            if (len > this.waveforms[0].get('sampleRate')*5)
                this.waveforms[0].clearSamples();
        },*/


        this.calcWFAttribs = function (waveforms) {

            var wfa = new Array();

            var regionHeight = this.canvas.height/waveforms.length;

            for (var w = 0; w < waveforms.length; w++) {
                wfa.push({
                    xPos: 0,    // The current (about to be plotted) calculated horizontal position [in pixels].
                    xPrev: 0,
                    yLoc: 0,    // The current (about to be plotted) calculated vertical location [in pixels].
                    yPrev: 0,
                    yOrg: 0,    // The vertical origin of the waveform [in pixels]. Center-of-region for ECG, bottom for others.
                    msSamp: 0,  // Number of milli-secs in a single sample point (non-downsampled).
                    xStep: 0,   // Combined calculation of horizontal scaling factors, to make drawing efficient [sample-rate -> pixels].
                    yStep: 0,   // Combined calculation of veritcal scaling factors, to make drawing efficient [sample value -> pixels].
                    cSamples: 0,    // Count of samples (non-downsampled) that have occurred in the elapsed time since an update.
                });

                var regionTop = regionHeight*w;
                var regionBottom = regionHeight*(w+1);

                wfa[w].msSamp = 1.0 / waveforms[w].getSampleRate() * 1000.0;  // ms per sample
                wfa[w].xStep = WFConfig.xScale * wfa[w].msSamp/1000.0 * WFConfig.pxPerMM;

            //    if (waveform.ID == 0) {   // ECG
                   wfa[w].yOrg = regionBottom - regionHeight/2;
                   wfa[w].yStep = waveforms[w].getResolution() * WFConfig.yScale * WFConfig.pxPerMM;// * waveforms[w].getScale();
            //    }
            //    else {
            //       wf.yOrg = regionBottom;
            //       wf.yStep = waveform.getResolution() * regionHeight / waveform.getScale();
            //    }
                wfa[w].yLoc = wfa[w].yOrg;
                wfa[w].yPrev = wfa[w].yLoc;

            //    if (this.config.scrollType == SCROLLTYPE.SCROLL)
            //        wfAttrib.xPos = this.canvas.width-25;
            }
            return wfa;
        }

        this.updateBufferInd = function (size) {

            this.context.fillStyle = COLORS.BLACK;
            this.context.fillRect(0,0,2,this.canvas.height);

            this.context.beginPath();
            this.context.strokeStyle = COLORS.ORANGE;
            this.context.moveTo(1,0);
            this.context.lineTo(1,size);
            this.context.stroke();
        }

        this.update = function(tmElapsed) {

            // if defined, call the interval update callback.
            if (this.intervalCallback != undefined) {
                this.intervalCallback(this);
            }

            var wfi = 0;    // Waveform index, for now...

            // Default values for the waveforms. This must be in here due to these
            // getting reset when the waveform canvas is resized.
            this.context.strokeStyle = WFConfig.wfColor[wfi];
            this.context.lineWidth   = 2;

            var bufSize = this.waveforms[wfi].getSamplesLength();

            if (++this.count < this.cResync) {
                this.bufTotal += bufSize;
            }
            else {
                var bufAvg = this.bufTotal / this.cResync;
                this.tmBias = this.calcBias(bufAvg)

                this.updateBufferInd(bufAvg);
//                console.log("Average: tmBias=" + this.tmBias + " BufSize=" + bufAvg + " tmAvg=" + tmAvg);

                this.count = 0;
                this.bufTotal = 0;
            }
            // Adjust elapsed time to keep buffer in desired range.
            tmElapsed += this.tmBias;

            // Update the erase bar.
            this.updateEBar(tmElapsed);

            // Draw tmElapsed worth of waveform points.
            for (var w = 0; w < this.wfAttribs.length; w++)
            {
                this.context.beginPath();
                this.context.strokeStyle = WFConfig.wfColor[w];
                this.updateWF (tmElapsed, this.waveforms[w], this.wfAttribs[w]);
                this.context.stroke();
            }
        }

        this.calcBias = function (bufAvg) {

            var tmBias = 0;

            if (bufAvg == 0)    // If buffer empty, no time bias.
               tmBias = 0;
            else if (bufAvg < 10)   // Buffer getting very low, slow-down effective dequeue rate.
               tmBias = -2;
            else if (bufAvg < 20)
               tmBias = -1;
            else if (bufAvg > 60) { // Buffer getting very full, progressively increase dequeue rate.
               tmBias = 10 * bufAvg/30;
               if (tmBias > 100)
                  tmBias = 100;
            }
            else if (bufAvg > 30)
               tmBias = 1;
            else
               tmBias = 0;  // Buffer in target zone, no time bias.

            return tmBias;
        }

        this.updateEBar = function (tmElapsed)
        {
            var xERight = this.wfAttribs[0].xPrev + (tmElapsed/1000*WFConfig.xScale+WFConfig.EBarWidth)*WFConfig.pxPerMM;
            if (xERight > this.canvas.width) {
                xERight = 2 + xERight - this.canvas.width;
                if (this.EBarPos > xERight)
                    this.context.clearRect(this.EBarPos, 0, this.canvas.width-this.EBarPos+1, this.canvas.height);
                this.EBarPos = 2;
            }
            this.context.clearRect(this.EBarPos, 0, xERight-this.EBarPos+1, this.canvas.height);
            this.EBarPos = xERight;
        }

        this.updateWF = function (tmElapsed, waveform, wfa)
        {
            var bConnect = true;

            this.context.moveTo(wfa.xPrev, wfa.yPrev);

            wfa.cSamples += tmElapsed / wfa.msSamp;   // Determine number of samples (full rate) that fit in elapsed time period.

            // If tmElapsed includes some samples, but there are none in buffer, simply update position and return.
            if (wfa.cSamples && waveform.getSamplesLength() <= 0) {
                wfa.xPos += wfa.cSamples*wfa.xStep;
                wfa.xPrev = wfa.xPos;
                wfa.cSamples = 0;
                return;
            }
            var samp = waveform.peekSample();
            while (wfa.cSamples && samp && (samp.length >= 2) &&
                    (samp[0] <= wfa.cSamples))  // This check insures the sample occurred inside the elapsed time frame.
            {
                samp = waveform.dequeueSample();
                if ((!samp) || (samp.length < 2))   // Each 'sample' must include both a data point and down-sampled position.
                    break;

                var dist = samp[0];
                var sample = samp[1];

                wfa.xPos += dist*wfa.xStep;
                wfa.yLoc = wfa.yOrg - sample*wfa.yStep;

                if (wfa.xPos >= this.canvas.width) {
                    wfa.xPos = 2.0; // Move to left-edge.
                    wfa.xPrev = 2.0;
                    bConnect = false;
                }

                if (bConnect)
                    this.context.lineTo(wfa.xPos, wfa.yLoc);
                else {
                    this.context.moveTo (wfa.xPos, wfa.yLoc)
                    bConnect = true;
                }

                wfa.xPrev = wfa.xPos;
                wfa.yPrev = wfa.yLoc;

                wfa.cSamples -= dist;    // Update remaining samples.
                if (wfa.cSamples && waveform.getSamplesLength() <= 0) {
                    wfa.xPos += wfa.cSamples*wfa.xStep;
                    wfa.xPrev = wfa.xPos;
                    wfa.cSamples = 0;
                    break;
                }
                samp = waveform.peekSample();
            }
            // Note that we can exit here with cSamples > 0. This is important to maintain the time-base.
        }

        this.getCanvas = function () {
            return this.canvas;
        }

        this.getSlotId = function () {
            return this.slotId;
        }
    }

    // Return the constructor function for this module.
    return WaveformRenderer;
});
