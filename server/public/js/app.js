define(['jquery',                          // Force jQuery UI to be loaded
        'unslider'
],
function($)
{
    // Calling noConflict(true) forces jQuery to remove all global jQuery
    // variables, namely '$' and 'jQuery', from the global scope. This
    // ensures that jQuery can only be referenced as a module (which is
    // loaded and managed by RequireJS).
    $.noConflict(true);
    $('.banner').unslider(
        {
            speed:500,
            delay:3000,
            key: true,
            fluid: false,
            dots: true
        }
    );




});
