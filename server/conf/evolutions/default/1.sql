# --- !Ups

CREATE TABLE TrainingInstitution (
  id int(20) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  gradeId int(20)  COMMENT 'relation with Grade table',
  address varchar(150),
  regionId int(20),
  phone varchar(20) NOT NULL,
  landline varchar(20) NOT NULL,
  adPic varchar(100) NOT NULL comment 'the picture for advertisement on index page',
  thumbnailPic varchar(100) NOT NULL,
  picture varchar(100) DEFAULT NULL,
  courseMapId varchar(20) DEFAULT NULL COMMENT ' Many to many to course table,',
  description varchar(250) NOT NULL,
  htmlInfo text,
  isHomePage int(11) DEFAULT '0' COMMENT 'is display on home page(0：not；1：yes',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 COMMENT='Training Institution';

CREATE TABLE Advertisement (
  id int(20) NOT NULL AUTO_INCREMENT,
  trainInstId int(20) NOT NULL,
  name varchar(50) NOT NULL,
  thumbnail varchar(100) NOT NULL,
  deadline date NOT NULL,
  description varchar(500) DEFAULT NULL,
  htmlInfo text,
  isPres tinyint(1) DEFAULT 0 COMMENT 'present on home page 0:not pres; 1:pres',
  PRIMARY KEY (id)
)  DEFAULT CHARSET=utf8 COMMENT='The advertisement of Training institution';


CREATE TABLE Course (
  id int(20) NOT NULL AUTO_INCREMENT,
  name varchar(50) NOT NULL,
  PRIMARY KEY (id)
)  DEFAULT CHARSET=utf8 COMMENT='Training Course such as drawing, dancing';

CREATE TABLE TrainingInstCourseMap (
  id int(20) NOT NULL AUTO_INCREMENT,
  courseId int(20),
  traingInstId int(20),
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 COMMENT='Relation table of traing institution and training Course';

CREATE TABLE Grade (
  id int(20) NOT NULL AUTO_INCREMENT,
  type int(1) NOT NULL DEFAULT '1' COMMENT '1：user；2：training institution；3：rate for training institution',
  grade int(11) DEFAULT '1' COMMENT 'garde, the bigger the better',
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 COMMENT='Grade Table';

CREATE TABLE Region (
  id int(20) NOT NULL AUTO_INCREMENT,
  province varchar(45) NOT NULL,
  city varchar(45) NOT NULL,
  district varchar(45) DEFAULT NULL,
  PRIMARY KEY (id)
)  DEFAULT CHARSET=utf8 COMMENT='region table';

CREATE TABLE UserRate (
  id int(20) NOT NULL AUTO_INCREMENT,
  userId int(20) NOT NULL,
  trainingInstId int(20) NOT NULL,
  date date NOT NULL,
  gradeId int(11) DEFAULT NULL COMMENT 'the relation table of Grade',
  comment varchar(500) DEFAULT NULL,
  PRIMARY KEY (id)
) DEFAULT CHARSET=utf8 COMMENT='User Rate for training institution'


CREATE TABLE User (
  id int(20) NOT NULL AUTO_INCREMENT,
  nickName varchar(45) DEFAULT NULL COMMENT 'nick name',
  avatar varchar(200) DEFAULT NULL COMMENT 'user avatar',
  phone varchar(20) NOT NULL,
  birthday date ,
  gender int(11) NOT NULL DEFAULT '0' COMMENT '1：male；2：femail',
  passWord varchar(45) NOT NULL,
  active int(11) NOT NULL DEFAULT '1' COMMENT '0：Not active；1：active；',
  regionId varchar(45) NOT NULL COMMENT 'user region',
  gradeId varchar(45) COMMENT 'user grade',
  PRIMARY KEY (id)
)  DEFAULT CHARSET=utf8 COMMENT='User Table'

# --- !Downs
DROP TABLE Advertisement;
DROP TABLE TrainingInstCourseMap;
DROP TABLE Course;
DROP TABLE TrainingInstitution;
DROP TABLE grade;